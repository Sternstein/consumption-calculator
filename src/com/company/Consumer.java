package com.company;

import java.util.*;


public class Consumer {
    String id;
    double consumption;
    double distibuted_consumption = 0;
    ArrayList<Consumer> childrenConsumers = new ArrayList<>();
    ArrayList<Consumer> childConsumersDistributed = new ArrayList<>();
    Consumer(String id_value, double value){
        this.id = id_value;
        this.consumption = value;
    }
    private void correct_distribution() {
        double current_consumption = 0;
        for (Consumer  consumerEntity: childrenConsumers) {
            current_consumption += consumerEntity.getAllConsumption();
        }
        System.out.println("Корректировка значений");
        double correctionValue = round_to_two( consumption - current_consumption);
        if (correctionValue > 0) {
            Collections.sort(childrenConsumers, Comparator.comparing(Consumer::getAllConsumption));
            Collections.reverse(childrenConsumers);
            Optional<Consumer> first = childrenConsumers.stream().findFirst();
            first.ifPresent(consumer -> consumer.setDistibuted_consumption(correctionValue));
        }
        for (Consumer  consumerEntity: childrenConsumers) {
            System.out.println("Потребление на дочернем узле " + consumerEntity.getId());
            System.out.println(consumerEntity.getAllConsumption());
        }
        for (Consumer  consumerEntity: childrenConsumers) {
            consumerEntity.distribute_consumption();
        }
    }
    public String getId() {
        return this.id;
    }
    public double round_to_two(double value) {
        return  (double) Math.round(value * 100) / 100;
    }
    public void setDistibuted_consumption(double value){
        this.distibuted_consumption += value;
    }
    public double getAllConsumption() {
        return this.consumption + this.distibuted_consumption;
    }
    public double getConsumption() {
        return this.consumption;
    }
    public void setChildrenConsumer(Consumer newChild){
        this.childrenConsumers.add(newChild);
    }
    public ArrayList<Consumer> getChildrenConsumers(){
        return this.childrenConsumers;
    }

    public void distribute_consumption() {
        double consumptionToDistibute = 0;
        double consumptionOfChildren = 0;
        double current_consumption = 0;
        double consumptionNode = this.getAllConsumption();
        if (childrenConsumers.isEmpty()) {
            System.out.println("Узел  " + id + " не имеет дочерних узлов");
        }
        else
            {
                System.out.println("Начинаем корректировку на узле " + id);
                for (Consumer consumerEntity : childrenConsumers) {
                    if (consumerEntity.getConsumption() > 0) {
                        consumptionOfChildren += consumerEntity.getAllConsumption();
                        childConsumersDistributed.add(consumerEntity);
                    }
                    current_consumption += consumerEntity.getAllConsumption();

                }
                System.out.println("Потребление на узле " + String.valueOf(consumptionNode));
                System.out.println("Показание по счётчикам на дочерних узлах " + String.valueOf(current_consumption));
                consumptionToDistibute = consumptionNode - current_consumption;
                if (consumptionToDistibute <= 0) {
                    System.out.println("Коррекция не нужна");
                } else {
                    String s = String.valueOf(consumptionToDistibute);
                    int i = 0;
                    System.out.println("Потребление для распределения: " + s);
                    for (Consumer consumerEntity : childConsumersDistributed) {
                        double x1 = (consumerEntity.getConsumption() / consumptionOfChildren) * consumptionToDistibute;
                        double x2 = round_to_two(x1);
                        consumerEntity.setDistibuted_consumption(x2);
                    }
                    correct_distribution();
                }
            }
    }
}
