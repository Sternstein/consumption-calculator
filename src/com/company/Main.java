package com.company;
import com.company.Consumer;

public class Main {
    public static void main(String[] args) {
        Consumer x = new Consumer("1",999.9);
        Consumer x11 = new Consumer("1.1",300);
        Consumer x12 = new Consumer("1.2",200);
        Consumer x13 = new Consumer("1.3",-70);
        Consumer x14 = new Consumer("1.4",300);

        Consumer x121 = new Consumer("1.2.1",150);
        Consumer x122 = new Consumer("1.2.2",50);
        x12.setChildrenConsumer(x121);
        x12.setChildrenConsumer(x122);

        Consumer x1411 = new Consumer("1.4.1.1",77);
        Consumer x1412 = new Consumer("1.4.1.2",46);
        Consumer x1413 = new Consumer("1.4.1.3",80);


        Consumer x141 = new Consumer("1.4.1",201);
        x141.setChildrenConsumer(x1411);
        x141.setChildrenConsumer(x1412);
        x141.setChildrenConsumer(x1413);
        Consumer x142 = new Consumer("1.4.2",33);
        x14.setChildrenConsumer(x141);
        x14.setChildrenConsumer(x142);

        x.setChildrenConsumer(x11);
        x.setChildrenConsumer(x12);
        x.setChildrenConsumer(x13);
        x.setChildrenConsumer(x14);

        System.out.println("Start test!");
        x.distribute_consumption();
    }
}
